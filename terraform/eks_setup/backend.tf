terraform {
  backend "s3" {
    bucket = "cicd1-terraform-eks1"
    region = "ap-southeast-1"
    key    = "eks/terraform.tfstate"
  }
}
